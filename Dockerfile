FROM alpine:3.18
RUN apk --no-cache add dnsmasq
EXPOSE 53/tcp
EXPOSE 53/udp
ENTRYPOINT ["dnsmasq", "-d", "--user=dnsmasq", "--group=dnsmasq"]
